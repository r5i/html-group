$(document).ready(function() {
	var gallery = $('.g-carousel');
	var sync2 = $(".gallery-carousel");
	var myWin = $(window).width();
	var addPad = '';
	var syncedSecondary = true;
	if(myWin<992){
		addPad = 0;
	}else{
		addPad = 100;
	}
	gallery.owlCarousel({
		center: false,
		items:1,
		loop:true,
		margin:0,
		autoplay: false,
		autoplayHoverPause:true,
		smartSpeed: 1000,
		lazyLoad: true,
		dots:false,
		nav:false,
	}).on('changed.owl.carousel', syncPosition);
	sync2.owlCarousel({
		center: true,
		items:3,
		stagePadding: addPad,
		loop:true,
		margin:0,
		autoplay: false,
		autoplayHoverPause:true,
		smartSpeed: 1000,
		lazyLoad: true,
		dots:true,
		nav:false,
	}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		//var current = el.item.index;

		//if you disable loop you have to comment this block
		var count = el.item.count-1;
		var current = Math.round(el.item.index - (el.item.count/2) - .5);

		if(current < 0) {
			current = count;
		}
		if(current > count) {
			current = 0;
		}
	}

	function syncPosition2(el) {
		if(syncedSecondary) {
			var number = el.item.index;
			gallery.data('owl.carousel').to(number, 100, true);
		}
	}

	sync2.on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).index();
		gallery.data('owl.carousel').to(number, 300, true);
	});


	// Custom Navigation Events
	$(".prev-container").click(function () {
		sync2.trigger('prev.owl.carousel');
	});

	$(".next-container").click(function () {
		sync2.trigger('next.owl.carousel');
	});
	$(".g-prev").click(function () {
		gallery.trigger('prev.owl.carousel');
	});

	$(".g-next").click(function () {
		gallery.trigger('next.owl.carousel');
	});
	$('.gallery-container').mouseenter(function(){
		$('.prev-container, .next-container').fadeIn();
	});
	$('.gallery-container').mouseleave(function(){
		$('.prev-container, .next-container').fadeOut();
	});
	$('.owl-item').click(function(){
		$('.g-modal-container').fadeIn();
	});
	$('.g-modal-bg, .g-modal-close').click(function(){
		$('.g-modal-container').fadeOut();
	});
});