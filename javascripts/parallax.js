$(document).ready(function() {
    $(window).bind('scroll', function(e) {
        parallax();
    });
});

function parallax() {
    var scrollPosition = $(window).scrollTop();
    $('#inner-banner').css('top',(0 - (scrollPosition * .7))+'px' );
    $('#para1').css('top',(0 - (scrollPosition * .1))+'px' );
    $('#para2').css('top',(0 - (scrollPosition * .4))+'px' );
    $('#para3').css('top',(0 - (scrollPosition * .4))+'px' );
    $('#para4').css('top',(0 - (scrollPosition * .4))+'px' );
    $('#para5').css('top',(0 - (scrollPosition * .4))+'px' );
}       