module NewsHelper

    def website_news
       @website.active_news("",[],[],2,'desc')
    end

    def announcement
      News.find_by_id(@webpage.request_params[:id])
    end

    def featured_news
      news = @website.active_news("",[],[]).select{|f| f.is_featured == true}
      news.take(2) unless news.blank?
    end

end
